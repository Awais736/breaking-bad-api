import React from "react";
import {BrowserRouter,Switch,Route} from 'react-router-dom';
import Home from './components/Cards/Home';
import Carddetails from './components/Cards/card-deails';


function App(){
  return (
        <BrowserRouter>
          <Switch>
            <Route exact path="/" component={Home}/>
            <Route exact path="/card/:id" component={Carddetails} />
            
          </Switch>
        </BrowserRouter>
  );

}
export default App;