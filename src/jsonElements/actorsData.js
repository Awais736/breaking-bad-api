export const actorsData = [
  {
    id:'01',
    actorImg:
      "https://vignette.wikia.nocookie.net/breakingbad/images/b/b3/StaceyEhrmantraut.png/revision/latest?cb=20150310150049",
    actorName: "Stacey Ehrmantraut",
    actorRelation: "Mike's daugter-in-law",
    seasons:"3",
    status:"Alive",
  },
  {
    id:'02',
    actorImg:
      "https://images.amcnetworks.com/amc.com/wp-content/uploads/2015/04/cast_bb_700x1000_hank-schrader-lg.jpg",
    actorName: "Henry Schrader",
    actorRelation: "Hank",
    seasons:"1,2,3,4,5",
    status:"Deceased",
  },
  {
    id:'03',
    actorImg:
      "https://vignette.wikia.nocookie.net/breakingbad/images/a/a4/Jake.jpg/revision/latest?cb=20121214201656&path-prefix=de",
    actorName: "Jake Pinkman",
    actorRelation: "Jake",
    seasons:"1",
    status:"Alive",
  },
  {
    id:'04',
    actorImg:
      "https://images.amcnetworks.com/amc.com/wp-content/uploads/2015/04/cast_bb_700x1000_badger-lg.jpg",
    actorName: "Brandon Mayhew",
    actorRelation: "Badger",
    seasons:"1,2,3,4,5",
    status:"Alive",
  },
  {
    id:'05',
    actorImg:
      "https://vignette.wikia.nocookie.net/breakingbad/images/f/fc/Adam_Pinkman.png/revision/latest?cb=20191106203119",
    actorName: "Adam Pinkman",
    actorRelation: "Jesse's dad",
    seasons:"1,2,3",
    status:"Alive",
  },
  {
    id:'06',
    actorImg:
      "https://vignette.wikia.nocookie.net/breakingbad/images/a/a1/DrDelcavoli.jpg/revision/latest/top-crop/width/360/height/450?cb=20170721163949",
    actorName: "Dr. Delcavoli",
    actorRelation: "Dr. Delcavoli",
    seasons:"1,2",
    status:"Alive",
  },
  {
    id:'07',
    actorImg:
      "https://i.pinimg.com/originals/73/8f/66/738f6698535ad570165427bd8d5b70b4.jpg",
    actorName: "Jane Margolis",
    actorRelation: "Apology Girl",
    seasons:"2",
    status:"Deceased",
  },
  {
    id:'08',
    actorImg: "https://s-i.huffpost.com/gen/1317262/images/o-ANNA-GUNN-facebook.jpg",
    actorName: "Skyler White",
    actorRelation: "Sky",
    seasons:"1,2,3,4,5",
    status:"Alive",
  },
  {
    id:'09',
    actorImg: 
      "https://m.media-amazon.com/images/M/MV5BMTkwMTkxNjUzM15BMl5BanBnXkFtZTgwMTg5MTczMTE@._V1_UY317_CR175,0,214,317_AL_.jpg",
    actorName: "George Merket",
    actorRelation: "ASAC Merkert",
    seasons:"2,3,4,5",
    status:"Alive",
  },
  {
    id:'10',
    actorImg: 
      "https://vignette.wikia.nocookie.net/breakingbad/images/a/a2/Tess_Harper.jpg/revision/latest?cb=20120923235754",
    actorName: "Mrs. Pinkman",
    actorRelation: "Jesse's Mom",
    seasons:"1,2,3",
    status:"Alive",
  }, 
  {
    id:'11',
    actorImg: 
      "https://pmctvline2.files.wordpress.com/2013/09/breaking-bad-elanor-anne-wenrich-325.jpg?w=325&h=240",
    actorName: "Holy White",
    actorRelation: "Holy",
    seasons:"2,3,4,5",
    status:"Alive",
  }, 
  {
    id:'12',
    actorImg: 
      "https://vignette.wikia.nocookie.net/breakingbad/images/5/51/Andrea_-_To%27hajilee.png/revision/latest?cb=20131025094457",
    actorName: "Andrea Cantillo",
    actorRelation: "Andrea",
    seasons:"3,4,5",
    status:"Deceased",
  },  
];
