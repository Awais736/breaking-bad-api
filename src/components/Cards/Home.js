import React from "react";
import Navbar from "../Navbar/Navbar";
import Footer from "../Footer/footer";
import "../../App.css";
import Title from "../Title/Title";
import Cards from "../Cards/Cards";
import { actorsData } from "../../jsonElements/actorsData";

function Home() {
  return (
    <section className="App">
      <header>
        <Navbar />
      </header>
      <main>
        <section className="detail-wrapper">
          <Title />
        </section>
        <section className="actors-card-section">
          {actorsData.map((cardData, index) => (
            <Cards
                id={index}
              cardImg={cardData.actorImg}
              name={cardData.actorName}
              relation={cardData.actorRelation}
            />
          ))}
        </section>
        <section>
            <Footer />
        </section>
      </main>
    </section>
  );
}

export default Home;
