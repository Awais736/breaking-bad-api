import React, {useEffect,useState} from 'react';
import Navbar from '../Navbar/Navbar';
import Footer from '../Footer/footer';
import {actorsData} from '../../jsonElements/actorsData';
import './card-details.css';

function Card({match}){
    const [data,setdata]=useState();
    useEffect(()=>{
        const character=actorsData[match.params.id];
        setdata(character);
    },[]);
    return(
        <section className="App">
            <header>
                <Navbar />
            </header>
            <section className="details-holder">
                {data && <img src={data.actorImg} className="card-image"/>}
                {data && <h2 className="format">{data.actorName}</h2>}
                {data && <span className="format">Appearances (Season's Wise): {data.seasons}</span>}
                <br />
                {data && <span className="format">Role: {data.actorRelation}</span>}
                <br />
                {data && <span className="format">Status: {data.status}</span>}
            </section>
            <section>
                <Footer />
            </section>
        </section>

    );

}
export default Card;
