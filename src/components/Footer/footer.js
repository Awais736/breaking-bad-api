import React from 'react';
import "./footer.css";

function Footer() {
    return(   
        <>     
            <div className="footer">
                <h2 className="Death">Click here to find out about a random death!</h2>
                <button href="#" className="btn">Death!</button>            
            </div> 
            <div className="line">
                <hr className="text-dark"></hr>
                <p className="season-details">Characters:63 Episodes:102 Quotes:70  Death Count:271</p>
            </div> 
        </>  
    );

}

export default Footer;